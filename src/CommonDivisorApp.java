import java.util.Scanner;

/**
 * Application that finds the greatest common divisor of two 
 * positive integers entered by the user.
 * @author your_username@email.uscb.edu
 * @version ICE for 25 Jan 2019
 */
public class CommonDivisorApp {
	
    /**
    * Main method for the app
    * @param args
    */
    public static void main(String[] args) 
	{
        /* MAIN METHOD - INITIALIZATION PHASE */
        // display a welcome message
        System.out.println("Greatest Common Divisor Finder");
        System.out.println();  // print a blank line
        // create the Scanner object
        Scanner sc = new Scanner(System.in);

        // initialize choice variable for continuing execution
        // ( make sure to initialize to a non-null value to 
        // prevent a nullpoint exception from being thrown) 
        // null pointer exceptions are vary hard to detect without testong 
        // because the compiler does not recognize exceptions
        String choice = "y"; // must be declared outside of the 
                       // loop in order to avoid sintax error
        
        /* MAIN METHOD - PROCESSING PHASE */
        // continue until choice isn't equal to "y" or "Y"
       do 
        {
            System.out.print("Enter first number: ");
            int firstNum = 0;
            try{
                firstNum = sc.nextInt();
                // we can throw your own exceptions for situations where 
                // the value entered was of the right data type but was 
                // not in the range for the running application
                if (firstNum <= 0) {
                    throw new Exception(); // instantiate and throws a 
                                           // generic exception object
                }
            } catch (Exception e) {
                System.err.println("Error! Invalid number -- please try again and enter a positive integer");
                sc.nextLine(); // discaed ant extra tokens
                continue; // goes to the loop continuation condition
            } // end try/catch
            
            System.out.print("Enter second number: ");
            int secondNum = 0;
            try {
                secondNum = sc.nextInt();
                if (secondNum <= 0) {
                    //throw new Exception();
                }
            } catch (Exception e) {
                System.err.println("Error! Invalid number -- please try again and enter a positive integer");
                sc.nextLine(); // discaed ant extra tokens
                continue; // goes to the loop continuation condition
            } // end try/catch

            // find result using gcd algorithm (defined in its own method)
	    int result = gcd(firstNum, secondNum);
			
            /* MAIN METHOD - MAIN LOOP TERMINATION PHASE */
            // display result to console using formatted printing
            System.out.printf("Greatest common divisor: %d\n", result);
            System.out.println(); // blank line for readability

            // see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.next();
            System.out.println();
        } while (choice.equalsIgnoreCase("y"));
		
        /* MAIN METHOD - TERMINATION PHASE */
		
    } // end method main
	
    /**
     * Returns the greatest common divisor if 2 integers, using 
     * Euclid's subtraction based algorithm
     * @param a the first integer
     * @param b the second integer
     * @return the greatest common divisor
     */
public static int gcd(int a, int b)
{
    /* GCD METHOD - INITIALISATION PHASE */
    // inisialize a variable for the gcd result
    int divisor = 0;
    
    /* GCD METHOD - PROCESSING PHASE */
    // continue until a = b 
    while (a != b)
    {
        // subtract a from b until b <= a 
        while (b > a)
        {
            b -= a; // b = b - a
        } // end inner while loop
        
        // swap the values of a and b
        divisor = a;
        a = b;
        b = divisor;
    } // end outer while loop
    
    /* GCD METHOD - TERMINATION PHASE */
    return divisor;
} // end staic method gcd
    
} // end class CommonDivisorApp

